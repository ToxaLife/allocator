// ALLOCATOR.cpp: определяет точку входа для консольного приложения.
//
 
// #include "stdafx.h"
 
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
typedef  struct list{
    struct list *prev, *next;
    size_t size;
    volatile char busy;
    volatile char flag;
} _T;
#define SIZE_OF_STRUCT      sizeof(_T)
#define SIZE_OF_RAM         10000000
#define NUMBER_OF_THREADS       20
#define NUMBER_OF_ITER      100000
#define asd printf("was here\n");
#ifdef __GNUC__
#define     synced_inc_Now  __sync_fetch_and_add(&Now, 1);
#define     synced_dec_Now  __sync_fetch_and_sub(&Now, 1);
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
 
void gdb();
static inline int spin_flag(_T* box, char needed){
    int i = 0;
    if (needed == 0){
        box->flag = needed;
        return 0;
    }
    do {
        __asm__ __volatile__(
            "lock xchg %1, %0"
            :"+m"(box->flag), "+r"(needed)
            );
    } while (needed);
    return 0;
}
static inline int spin_w(_T* box, char needed){
    if (needed == 0){
        box->flag = needed;
        return 0;
    }
    needed = 1;
    __asm__ __volatile__(
        "lock xchg %1, %0"
        :"+m"(box->flag), "+r"(needed)
        );
    return needed;
}
#elif defined _MSC_VER
#include <time.h>
#include <Windows.h>
#include <process.h>
#include <intrin.h>
#define     synced_inc_Now  Now++;
#define     synced_dec_Now  Now--;
 
inline int spin_flag(_T* box, char needed){
    volatile char *addr = &box->flag;
    if (needed != 1){
        box->flag = needed;
        return 0;
    }
    __asm  {
    a:  mov al, needed
        mov ebx, addr
        xchg    al, byte ptr[ebx]
        test    al, al
        jnz a
    }
    return 0;
}
inline int spin_w(_T* box, char needed){
    volatile char *addr = &box->flag;
    if (needed != 1){
        box->flag = needed;
        return 0;
    }
    __asm  {
        mov al, needed
            mov ebx, addr
            xchg    al, byte ptr[ebx]
            mov     needed, al
    }
    return needed;
}
#endif
 
int start = 1;
int done = 1;
volatile int x = 0;
volatile long Now = 0;
volatile char ram[SIZE_OF_RAM];
_T *as;//start & end
void gdb(){
    int i = 0;
    _T* l = as;
    unsigned long TOTAL_SIZE = 0;
    goto local_1;
    while (l != as){
    local_1:
        i++;
        TOTAL_SIZE = TOTAL_SIZE + SIZE_OF_STRUCT + l->size;
        printf("current=%p, next=%p, prev=%p, size=%ld, busy=%d, flag=%d\n", l, l->next, l->prev, l->size, l->busy, l->flag);
        l = l->next;
    }
    printf("TOTAL_SIZE=%lu\n", TOTAL_SIZE);
    printf("SIZE_OF_STRUCTS=%lu\n", (i)*SIZE_OF_STRUCT);
    for (;;) {}
}
 
void* _malloc(size_t n){
    _T *l = as->next;
    spin_flag(l, 1);
    while (l != as){
        if (l->busy == 0 && n <= l->size){
            if (l->size - n <= SIZE_OF_STRUCT){
                l->busy = 1;
                spin_flag(l, 0);
                return (void*)((char*)l + SIZE_OF_STRUCT);
            }
            l->size = l->size - n - SIZE_OF_STRUCT;
            _T *em = (_T*)((char*)l + SIZE_OF_STRUCT + l->size);
            em->flag = 1;
            em->size = n;
            em->busy = 1;
            em->prev = l;
            if (l->next != as){
                spin_flag(l->next, 1);
                l->next->prev = em;
                spin_flag(l->next, 0);
            }
            else {
                l->next->prev = em;
            }
            em->next = l->next;
            l->next = em;
            spin_flag(l, 0);
            spin_flag(em, 0);
            return (void*)((char*)em + SIZE_OF_STRUCT);
 
        }
        else {
            spin_flag(l->next, 1);
            l = l->next;
            spin_flag(l->prev, 0);
        }
    }
    printf("need %lu\n", n);
    gdb();
    return NULL;
}
void _free(void* a){
    if (!a){
        return;
    }
    int res;
    _T *l = (_T*)((char*)a - SIZE_OF_STRUCT);
    _T *temp;
here_1:
    spin_flag(l, 1);
    res = spin_w(l->prev, 1);
    if (res){
        spin_flag(l, 0);
        goto here_1;
    }
    temp = l->prev;
    l->busy = 0;
    if (temp->busy == 0){
        if (l->next != as){
            spin_flag(l->next, 1);
            l->next->prev = temp;
            temp->next = l->next;
            spin_flag(l->next, 0);
        }
        else {
            l->next->prev = temp;
            temp->next = l->next;
        }
        temp->size = temp->size + SIZE_OF_STRUCT + l->size;
        l = temp;
    }
    else {
        spin_flag(temp, 0);
    }
    spin_flag(l->next, 1);
    if (l->next != as){
        temp = l;
        l = l->next;
        if (l->busy == 0){
            if (l->next != as){
                spin_flag(l->next, 1);
                l->next->prev = temp;
                temp->next = l->next;
                spin_flag(l->next, 0);
            }
            else {
                l->next->prev = temp;
                temp->next = l->next;
            }
            temp->size = temp->size + SIZE_OF_STRUCT + l->size;
            spin_flag(temp, 0);
        }
        else {
            spin_flag(l, 0);
            spin_flag(temp, 0);
        }
    }
    else {
        spin_flag(l, 0);
        spin_flag(l->next, 0);
    }
    return;
}
#ifdef _MSC_VER
unsigned __stdcall
#elif  __GNUC__
void*
#endif
cycle(void *arg){
    int i, n = (int)arg;
    n=NUMBER_OF_ITER;
    synced_inc_Now
    while (!x);
    printf("here %d\n", start++);
    for (i = 0; i<n; i++){
        if (i % 1000 == 0) printf("%d\n", i);
        int *b = (int*)_malloc(100 * sizeof(int));
        memset(b, -1, 100 * sizeof(int));
        char *c = (char*)_malloc(500 * sizeof(char));
        memset(c, -1, 500 * sizeof(char));
        _free(c);
        long *a = (long*)_malloc(100 * sizeof(long));
        memset(a, -1, 100 * sizeof(long));
        _free(a);
        _free(b);
    }
    printf("done %d\n", done++);
    synced_dec_Now
    return NULL;
}
 
int main(){
    as = (_T *)ram;
    as->busy = 1;
    as->size = 0;
    as->flag = 0;
    _T *temp = (_T*)((char*)as + SIZE_OF_STRUCT);
    as->next = temp;
    as->prev = temp;
    temp->busy = 0;
    temp->flag = 0;
    temp->size = SIZE_OF_RAM - 2 * SIZE_OF_STRUCT;
    temp->next = as;
    temp->prev = as;
    int n = NUMBER_OF_ITER, check, i;
    float t_res;
#ifdef __GNUC__
    pthread_t *threads;
    struct timeval t1, t2;
    threads = (pthread_t*)malloc(NUMBER_OF_THREADS*sizeof(pthread_t));
    printf("STARTED\n");
    for (i = 0; i<NUMBER_OF_THREADS; i++){
        check = pthread_create(&threads[i], NULL, &cycle, &n);
        if (check) return 1;
    }
    while (Now != NUMBER_OF_THREADS);
    gettimeofday(&t1, NULL);
    x = 1;
    while (Now);
    gettimeofday(&t2, NULL);
    for (i = 0; i<NUMBER_OF_THREADS; i++){
        check = pthread_join(threads[i], NULL);
        if (check) return 1;
    }
    free(threads);
    printf("\nDone\n\n");
    t_res = ((t2.tv_sec*1.e6 + t2.tv_usec) - (t1.tv_sec*1.e6 + t1.tv_usec)) / 1.e6;
#elif defined _MSC_VER
    HANDLE *threads;
    clock_t t1, t2;
    threads = (HANDLE*)malloc(NUMBER_OF_THREADS*sizeof(HANDLE));
    printf("STARTED\n");
    for (i = 0; i<NUMBER_OF_THREADS; i++){
        threads[i] = (HANDLE)_beginthreadex(NULL, 0, cycle, (void*)n, 0, NULL);
    }
    while (Now != NUMBER_OF_THREADS);
    t1 = clock();
    x = 1;
    while (Now);
    t2 = clock();
    WaitForMultipleObjects(NUMBER_OF_THREADS, threads, TRUE, INFINITE);
    for (i = 0; i<NUMBER_OF_THREADS; i++){
        CloseHandle(threads[i]);
    }
    free(threads);
    printf("\nDone\n\n");
    t_res = (float)(t2 - t1) / CLOCKS_PER_SEC;
#endif
    printf("\n%d threads with %d interations in %g seconds\n", NUMBER_OF_THREADS, n, t_res);
    printf("Call gdb? y/n\n");
    char s = getchar();
    if (s == 'y'){
        gdb();
    }
    getchar();
    return 0;
}